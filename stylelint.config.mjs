export default {
  extends: ["stylelint-config-standard-scss", "stylelint-prettier/recommended"],
  rules: {
    "font-family-name-quotes": "always-unless-keyword",
    "alpha-value-notation": "number",
    "media-feature-range-notation": "prefix",
    "scss/no-global-function-names": null,
    "color-function-notation": null
  }
};
